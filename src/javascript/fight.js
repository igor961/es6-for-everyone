export function fight(firstFighter, secondFighter) {
  while (true) {
    if (firstFighter.health > 0) 
      makeDamage(firstFighter, secondFighter);
    else
      return secondFighter;
    if (secondFighter.health > 0)
      makeDamage(secondFighter, firstFighter);
    else
      return firstFighter;
  }
}

function makeDamage(attacker, enemy) { 
    enemy.health -= getDamage(attacker, enemy);
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return Math.max(0, damage);
}

export function getHitPower(fighter) {
  const r = Math.random() + 1;
  return fighter.attack * r;
}

export function getBlockPower(fighter) { 
  const r = Math.random() + 1;
  return fighter.defense * r;
}
