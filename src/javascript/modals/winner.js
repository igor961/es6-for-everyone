import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  const title = 'Winner';
  const bodyElement = createWinnerInfo(fighter);
  showModal({ title, bodyElement });
}

function createWinnerInfo(fighter) {
  const { name, source } = fighter;
  const winnerInfo = createElement({ tagName: 'div', className: 'modal-body' });
  const html = `
    <div class="winner-info">
      <h3>${name}</h3>
      <img src="${source}" alt="">
    </div>
  `;
  winnerInfo.insertAdjacentHTML('beforeend', html);
  return winnerInfo;
}
