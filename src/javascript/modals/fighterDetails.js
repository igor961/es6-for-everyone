import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, health, defense, attack, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

  const html = `
    <div class="fighter-card">
      <img src="${source}" width="30%" alt="">
      <table>
        <thead>
          <tr>
            <th colspan="2">${name}</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Health</td>
            <td>${health}</td>
          </tr>
          <tr>
            <td>Attack</td>
            <td>${attack}</td>
          </tr>
          <tr>
            <td>Defense</td>
            <td>${defense}</td>
          </tr>
        </tbody>
      </table>
    </div>
  `;
  fighterDetails.insertAdjacentHTML('beforeend', html);
  return fighterDetails;
}
